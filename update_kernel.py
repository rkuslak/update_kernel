#!/usr/bin/python3

import pathlib
import gzip

FIRMWARE_PATH = pathlib.Path("/boot", "firmware")
CURRENT_UNCOMPRESSED_KERNEL_PATH = FIRMWARE_PATH / "vmlinux"
CURRENT_COMPRESSED_KERNEL = FIRMWARE_PATH / "vmlinuz"

def check_if_kernel_outdated() -> bool:
    """
    If the uncompressed version of the current kernel does not match the
    GZipped version, decompress and store the new kernel.
    """
    with CURRENT_COMPRESSED_KERNEL.open("rb") as compressed_infile:
        compressed_kernel_bytes = compressed_infile.read()
    uncompressed_new_kernel_bytes = gzip.decompress(compressed_kernel_bytes)

    uncompressed_kernel_bytes = None
    if CURRENT_UNCOMPRESSED_KERNEL_PATH.exists():
        with CURRENT_UNCOMPRESSED_KERNEL_PATH.open("rb") as uncompressed_infile:
            uncompressed_kernel_bytes = uncompressed_infile.read()

    if (uncompressed_kernel_bytes is None
        or uncompressed_kernel_bytes != uncompressed_new_kernel_bytes
    ):
        backup_path = pathlib.Path(CURRENT_UNCOMPRESSED_KERNEL_PATH)
        backup_name = backup_path.name + ".bak"
        backup_path = backup_path.with_name(backup_name)

        CURRENT_UNCOMPRESSED_KERNEL_PATH.replace(backup_path)

        with CURRENT_UNCOMPRESSED_KERNEL_PATH.open("wb") as out_file:
            out_file.write(uncompressed_new_kernel_bytes)


if __name__ == "__main__":
    check_if_kernel_outdated()
