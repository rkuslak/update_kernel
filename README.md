Update Kernel
===

This exists as I use several Raspberry Pis with Ubuntu for various purposes
using external SSDs as the boot device and want to ensure they remain capable of
booting. With the OS residing on external storage you need to ensure the kernel
is decompressed for the bootload function; however Ubuntu defaults to using a
compressed kernel and does not automatically decompress it on installing a new
one.

Installation
===
You can activate this package by moving the dpkg hook (99decompress-kernel) into
the `/etc/apt/apt.conf.d` directory, and by default we expect the kernel update
script to be in `/boot/fireware`, though changing this is both highly
recommended and should be fairly obvious to achieve. Note the implementation is
intentionally naive, choosing to value ensuring the kernel is in a "valid" state
over execution time. In theory there are several obvious means this could be
made faster.

External Storage as Boot Device Set Up Expectations
===
Note, for a given Pi device to be bootable from external storage, it will need
to update the `/boot/firmware/config.txt` file to point at the uncompressed
kernel, otherwise it will fail to boot as the default Pi boot loader does not
support a compressed kernel. You can do this by updating the following line for
your Pi's architectures `kernel` target to point to the uncompressed kernel, and
adding the following config line:

```
kernel=vmlinux
initramfs initrd.img followkernel
```
